# Gitolite administration, sub-administration

## Description

**"Gitolite allows you to setup git hosting on a central server, with fine-grained access control and many more powerful features."** from [Gitolite](http://gitolite.com/gitolite/index.html)

Gitolite is setup such that each user in honors year and above may administer their own repositories.

  __Note:__ If you are a staff member, Msc or Phd student, you will be able to use your normal user name here. Chat to the administration person to add you.

1. What each student will have:
    * A `gitolite-admin` git repository.

    Honors students can create and manage access to their own repositories here, with the configuration file under `conf/sub/<student_number>.conf`. Each student will only be able to edit this one file. No access will be granted to other configuration files, nor will other files be created or removed.

    * All their git repositories.

    As defined in the file above.


## Accessing the administration repository

* You can clone the administration repository with `git clone hons-<year>@git.sun.ac.za:gitolite-admin`
* Remember, you will only be able to edit `conf/sub/<student_number>.conf`. Any other edits will cause a rejection from the server.
* Once you have added your desired repositories and pushed back to the server, they will be created for you automatically.

### Configuration file example
```
#Repos
repo 16137329/Documentation
  		RW+         = 16137329

repo 16137329/rw711
  		RW+         = 16137329
```
  __Warning:__ Under no circumstances should you create a repository without the naming convention `<student_number/<repo_name>`. If you try create a repository named`<student_number>` you will then only have one repository, or a repository of repositories, which is a bad idea.

In the above example, `16137329` has created a repository at `16137329/Documentation`, and another at `16137329/rw711`. `16137329` is also the only user with access to each repository.


## Accessing your repositories:

You would access the above example repository `16137329/Documentation` with the clone command:
```
git clone hons-2016@git.sun.ac.za:16137329/Documentation
```
Thus the generic would be:
```
git clone hons-<year>@git.ac.za:<student_number>/<repo>
```


## Work in progress

This wiki is a work in progress. Please mail suggestions/questions to the administrator.


## Good places to learn about gitolite:

* [Project home page](http://gitolite.com/gitolite/index.html)
* [Sub-admin](http://gitolite.com/gitolite/deleg.html)
