# Using the University GitLab server
## Signing in
All students and staff may sign in to the GitLab server, by using their university credentials, via the __US Single Sign-On__ button under the normal login area.

You may setup or reset your GitLab account password once you are signed in, if you want. This password will be separate from your SUN password. If you set no password, that is also okay.

## Email Address
In order for the server to tell you about things, like changes in projects you are watching, and places where you are mentioned, you should add, and verify an email address. You can add this on the first page that you encounter after logging in the first time, or, in your profile settings later on.

  __Note__: You will not be able to use GitLab without setting and verifying at least one email address.

## Managing your account
You may want to add some personal details to your account, like your name and a profile picture. Since it would be good to help identify you and your code to others. However, you may leave everything at default if you wish.

As you will see once logged in, you will want to add an [`SSH`](http://linuxcommand.org/man_pages/ssh1.html) key to your account. This will enable you to interact with your code.

Follow [this guide](gen-add.md) if you need some help.

## Other GitLab related usage
There is also plenty of info on how to use GitLab in the [GitLab docs](https://git.cs.sun.ac.za/help). Otherwise, don't hesitate to contact the wiki writer!
