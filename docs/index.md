# Home

Welcome to the CS admin docs. 

Here you will find all manner of help with items related to the technical details related to the CS courses and servers. 

See the menu on the left for navigation. 

Other useful sites:

* **CS website:** https://www.cs.sun.ac.za
* **NARGA website:** https://narga.sun.ac.za
* **IT and NARGA Service Desk:** https://servicedesk.sun.ac.za
* **Main University Student Website:** https://my.sun.ac.za
