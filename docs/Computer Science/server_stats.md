# Server Stats

Below are some general sever staticstics for the CS cluster. 

Usage is set to the last 24 hours. If you would like to see more, you can view the full Grafana app [here](https://grafana.proj.cs.sun.ac.za).

!!! note ""
    You need to be logged into [CS GitLab](https://git.cs.sun.ac.za) first, before the below will load.

## CPU
<iframe src="https://grafana.proj.cs.sun.ac.za/d-solo/0RpY5MqWk/node-exporter-summary?orgId=1&refresh=15m&from=now-24h&to=now&var-job=cs_servers&var-node=All&panelId=77" width="100%" height="450" frameborder="0"></iframe>

## RAM
<iframe src="https://grafana.proj.cs.sun.ac.za/d-solo/0RpY5MqWk/node-exporter-summary?orgId=1&refresh=15m&from=now-24h&to=now&var-job=cs_servers&var-node=All&panelId=78" width="100%" height="450" frameborder="0"></iframe>

## GPU
<iframe src="https://grafana.proj.cs.sun.ac.za/d-solo/0RpY5MqWk/node-exporter-summary?orgId=1&refresh=15m&from=now-24h&to=now&var-job=cs_servers&var-node=All&panelId=154" width="100%" height="450" frameborder="0"></iframe>
