# Welcome to CS GitLab!

## Pre-requisites for effective usage

1. Make sure you set and verify at least one email address in your profile. It is advised to use your university address. 
1. Setup your SSH keys. See the [Git Beginners Guide](git-beginners.md) for a start. 

## F.A.Q

### Q: How do I use the "US Office365" login instead of the "US Single Sign-On" login?
* If you have never logged in before, simply login with the **US Office365** button, and your account will be created for you.
* The **US Single Sign-On** button will be removed at the beginning of next year. Please see this [link to the depreciation notice by GitLab](https://docs.gitlab.com/ee/update/deprecations.html#cas-omniauth-provider).
* If you have not started using the **US Office365** login option, please see [the guide on how to get it working](https://git.cs.sun.ac.za/help/integration/omniauth.md#enable-omniauth-for-an-existing-user) as you will not be able to set this up yourself in the new year.

!!! info "Sanity Check"
    You should see *Disconnect US Office365* on your [Profile -> Account](https://git.cs.sun.ac.za/-/profile/account) page if the sign-in method is working.
    
    ![US Office365 button, saying "Disconnect" indicating it's connected.](/assets/images/profile_accounts.png)


### Q: Why can't I change my 'username'?
We use your username to identify you in scripts and other automation tasks. Thus, it needs to stay consistent and predictable. That is why we set it as your US username or US number and don't allow modification there of. 

!!! note
    You may change your display name if desired.


### Q: Do you have some useful resources to read up about how to use git?
1. See the [Git Beginners Guide](git-beginners.md) for a start.
1. Have a look at our home grown [PDF about advanced git usage](/assets/pdf/git.pdf) to help you through the more complex tasks when working with git. 
1. A good reference summary [here.](https://about.gitlab.com/images/press/git-cheat-sheet.pdf)
1. Another good guide is [here](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html)

### Q: How do the notifications work in GitLab?

!!! important
    Make sure your email address is set in the [User Emails](https://git.cs.sun.ac.za/-/profile/emails) section, and that you have chosen your preferred email account under [Notifications](https://git.cs.sun.ac.za/-/profile/notifications).

Notifications are there to help you keep up with whats happening in GitLab, but they are setup by default to not send more emails than what seems necessary. 

For example, if you *push* to a project, you will not get an email about it, nor will you get an email if your CI pipeline succeeds. 
However, if someone else creates an issue and mentions you, or your pipeline fails on your project, you will receive an email notification. 


If you want to change the default behavior, there are 2 places to do this:

1. Globally (for all your projects)

    You can set your defaults here: [Notification Settings](https://git.cs.sun.ac.za/-/profile/notifications)

2. Per group, or project

    In each project, you can select how verbose you want the notifications to be, by selection the bell icon on the project or group. See the following picture for an example.
    
    ![Notification Settings](/assets/images/notification-settings.png)
