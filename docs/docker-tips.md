# Docker tips

!!! note
    These tips and tricks can be used for docker anywhere, however they are aimed at users of the Computer Science shared servers.

!!! warning
    At the time of writing, Docker requires users to either have elevated privileges or for users to be in the `docker` group. 
    In short, a user who can run Docker containers also has implied "elevated privileges". 
    
    Therefore, users will only be allowed to use Docker and the shared servers if they have a good reason to do so.

    They should also be careful with their use of Docker on shared hosts. 

## Running containers

If you know that you do not need the docker container once its stopped, you can use the `--rm` flag when executing `docker run ...`.
This will automatically remove the container from the host once it has stopped.

It's probably good practice to get into the habit of using this, since it forces a user to make sure any important info and data is stored in a volume. 

The biggest drawback here is that you will not be able to read the container log once the container stops, since it will have been removed.

## Image and Container Naming
It is very important to keep things organised on a shared server resource, especially with Docker. 
That is because it is not simple to tell who ran or built what with Docker. Most processes related to docker look like the `root` user ran them.

Therefore, all images and containers should be named something that includes the users US username. 
You should note the following implications when **not** naming your images and/or containers. 

* All images and containers not named **may** be removed in order to free up space if needs be, before properly named images and containers are considered.
* If a docker container is using up an unreasonable[^1] percentage of resources, and it has not been named in an intuitive way, the system admin will not know who to contact in order to follow up. 
  As such, you will not only incur the ire of said system admin, but your container **may** be stopped before the work that was planned is finished.

??? note "But, `LABELs` ?"
    I have chosen not to use the `LABEL` docker facility for this, as it does not give immediate information to the user.
    For example, when running `docker ps`, one can immediately see who owns what if containers and images are named correctly. 
    Feel free to use this for your own use though[^ref1].

### Image Naming
If an image has been pulled from a remote registry, it will already have a name. This is OK, since you can just pull that image again if it's removed. 

However, if the image was built locally (from a Dockerfile for example) the image name will need to be set at build time.

This can be done on the command line[^ref2]:
``` bash
docker build --tag <name[:tag]> <context>
```

For example:
``` bash
docker build --tag ajcollett:v2.1 .
```

### Container Naming
There are a few ways to name containers, however we will only cover the simplest here.

??? note
    The more comprehensive way of doing things is to use Docker Compose[^ref3], especially if you have multiple containers working together.

In order to add a name to a container when you run it use the following `docker run` option[^ref4]:
``` bash
docker run --name <your username + extra helpful things> <image name>
```

For example:
``` bash
docker run --name ajcollett_pythonapp_v1 -it python:latest
```

## Cleaning up 

It is helpful for users to cleanup old containers and images they may have created, if they have not used `--rm` when running containers.
However, this can be tricky on a shared system, especially if you haven't named your objects properly.

!!! warning
    Commands like `docker <object> prune`[^2], apply system wide. So, although they sound safe, they may remove objects others still need.

    *Do not* prune the system object. 


*W.I.P*


[^1]: In this context, "unreasonable" implies something is likely wrong with the container: storage or RAM may be filling up, or the CPU/GPU maybe running at high load for a long time.
[^2]: Where `<object>` is either `image`, `volume`, `container`, `system` or `network`.


[^ref1]: https://docs.docker.com/config/labels-custom-metadata/
[^ref2]: https://docs.docker.com/engine/reference/commandline/build/
[^ref3]: https://docs.docker.com/compose/
[^ref4]: https://docs.docker.com/engine/reference/commandline/run/