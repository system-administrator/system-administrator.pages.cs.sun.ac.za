# GitLab website deployment
GitLab has the ability to use continuous integration (CI). Meaning, one can run actions on every, or selected, commits.

In this case, we use the power of CI to cleanup and auto-deploy websites on Star, and WWW0.

## Setup
The system administrator of `git.cs` will need to be involved, as well as the web-server administrator.

At this point, a runner will be setup for you, and linked to your project, and a basic .gitlab-ci.yml file will be provided to help setup the site. This file will be explained a little later.

### Steps - the high level process
Once setup, below are the high level steps of what will happen at each commit to the repo.

1. Commit comes in to git.cs.sun.ac.za.
2. GitLab will contact the runner.
3. The runner will:
  1. SSH to the web-server.
  2. Clone or pull, depending on whether the repo exists.
  2. Then execute the scripts within the .gitlab-ci.yml file.
  
  __Note__: The scripts usually just rsyncs the contents of the `public_html` directory of the repo to the `public_html` directory of the webserver, then execute a number of commands to set permissions correctly. The executing user has not got super user privileges.

4. Leave the webserver and report success or failure to the git.cs.sun.ac.za frontend.


## Steps for the website maintainer
There are two places that the user can maintain the website from.

### Users PC
With the power of git, a website maintainer can have a local copy of the repo, and
push changes to the remote copy with git. This way they never need to ssh into anywhere.

To do this, simply follow the usual git way of `cloning`, `adding` changes, `committing` them, then
`pushing` those changes to the remote. The GitLab server will deal with the rest.

### Within [GitLab](https://git.cs.sun.ac.za)
Another place to edit ones website files is through the browser.

Any file in the project can be edited from a simple text editor in the browser.
New files can be uploaded or replace old files.

So, all small changes can be made in the browser quite easily. 
