# Cloning from the Computer Science Gitolite repository

1. In terminal, [navigate](http://linuxcommand.org/lc3_man_pages/cdh.html) to a directory that you'd like to store your git repository in.

2. [Clone](http://git-scm.com/docs/git-clone) your repository:

First though, a few points about the GitLab server.
1. We use the *name_space* of a project to help indicate who it belongs to. This is the first part of the __directory__ location in the URL.
For students, this will likely be the *course_year* combination, or for other repos, your *student_number*. For post-graduate or staff, this may be a *course_year* or your *user_name* that __should__ be the same as your Stellenbosch University username.
2. The second part of the __directory__ location in the URL is your git project name, or repo name.
For undergrad, this will likely be setup by your lecturer, and be something like *project1*. For post-graduate and staff, this will be whatever you named the repo when creating it. 

3. `git clone git@git.cs.sun.ac.za:<name_space>/<git project>`

  __Note__: Don't forget to replace the words between `<>` with your info, and to omit the `<>`. e.g. `16137329` or `rw711-2016` to replace `<name_space>`
