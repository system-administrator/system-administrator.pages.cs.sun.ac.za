# System Admin Private Documentation

[Link](http://system-administrator.pages.cs.sun.ac.za/sysadm-private-docs) 
This site contains documentation regarding the responsibilities of the system admin as well as relevant "how tos".

## Quick Access

This table contains the links of the relevant platforms used as a system administrator.

| Platform | Description |
| --- | --- | 
| [Jira](https://servicedesk.sun.ac.za/jira/secure/Dashboard.jspa) | This is the platform where all tickets are handled |
| [Pomerium](https://console.cs.sun.ac.za/app/reports/traffic) | This is used to grant access to different platforms |
| [GitLab](https://git.cs.sun.ac.za/system-administrator) | This is the GitLab group "System Administrator" that contains relevant projects |
| [Rancher](https://rancher.k8s.cs.sun.ac.za/dashboard/home) | Software product to manage Kubernetes clusters |
| [Longhorn](https://rancher.k8s.cs.sun.ac.za/k8s/clusters/c-m-twr8nw6g/api/v1/namespaces/longhorn-system/services/http:longhorn-frontend:80/proxy/#/dashboard) | Within Rancher and can be used to manage volumes relating to storage |