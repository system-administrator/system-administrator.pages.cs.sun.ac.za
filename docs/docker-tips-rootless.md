# Docker tips - for rootless Docker

!!! warning
    Rootless docker is in "testing" on the CS servers and desktops. This will only apply to certain users. This is a W.I.P

!!! note
    To find out more info on rootless Docker, and whether you will encounter issues with it, please see the below link.

    * https://docs.docker.com/engine/security/rootless/

Rootless Docker is a way of running the Docker daemon in the users own namespace, and does not require the user to be in a special group, or to have sudo permissions. However, it has limitations (see above) and requires an initial setup. On the CS machines, for the moment, the admin will need to setup each user to be able to use rootless Docker. This will be automated in future. 

## Initialize

1. Test that you are present in the `/etc/subguid` and `/etc/subuid` files. There will be output containing your username:
    ``` bash
    grep ^$(whoami): /etc/subuid
    grep ^$(whoami): /etc/subgid
    ```
2. If you are not in that file, you will need to contact the admin for help. If you are then run:
    ``` bash
    dockerd-rootless-setuptool.sh install
    ```
    * Read the output carefully. If successful, follow the instructions about environment variables you should set in your `.bashrc` file.
    * If not, then there should be some output that you can send to the system admin.

3. If everything went well, run the following to make sure your Docker daemon stays alive, and is started on boot:
    ``` bash
    systemctl --now --user enable docker
    loginctl enable-linger $(whoami)
    ```
4. Finally, test that you can use Docker:
    ``` bash
    docker run hello-world
    docker ps
    docker images ls
    ```

!!! important
    Docker is running and storing all it's data in your home directory. This means you will need to take note of how much space you are using. On shared machines, this means each user will have a copy of each image they download, unlike with the normal way of running Docker, where the images and containers are saved once for all. 

## Running containers

If you know that you do not need the docker container once its stopped, you can use the `--rm` flag when executing `docker run ...`.
This will automatically remove the container from the host once it has stopped.

It's probably good practice to use this, since it forces a user to make sure any important info and data is stored in a volume. 

The biggest drawback here is that you will not be able to read the container log once the container stops, since it will have been removed.

## Naming

### Images
If an image has been pulled from a remote registry, it will already have a name. This is OK, since you can just pull that image again if it's removed. 

However, if the image was built locally (from a Dockerfile for example) the image name will need to be set at build time.

This can be done on the command line[^ref2]:
``` bash
docker build --tag <name[:tag]> <context>
```

For example:
``` bash
docker build --tag ajcollett:v2.1 .
```

### Containers
There are a few ways to name containers, however we will only cover the simplest here.

??? note
    The more comprehensive way of doing things is to use Docker Compose[^ref3], especially if you have multiple containers working together.

In order to add a name to a container when you run it use the following `docker run` option[^ref4]:
``` bash
docker run --name <your username + extra helpful things> <image name>
```

For example:
``` bash
docker run --name ajcollett_pythonapp_v1 -it python:latest
```

## Cleaning up 

It is helpful for users to cleanup old containers and images they may have created, if they have not used `--rm` when running containers.

!!! warning
    Commands like `docker <object> prune`[^2], apply to your docker instance. 
    
    However, **make sure you are using rootless docker before running this command.**

    * Test if you are indeed using rootless Docker by running `docker info | grep "Docker Root Dir"`.
    * If the output contains `/home/<username>/.local/share/docker` then you are using rootless Docker. 
    * If it contains `/var/lib/docker` then you are using the system Docker daemon, and you should NOT run the prune commands. 
    

*W.I.P*


[^1]: In this context, "unreasonable" implies something is likely wrong with the container: storage or RAM may be filling up, or the CPU/GPU maybe running at high load for a long time.
[^2]: Where `<object>` is either `image`, `volume`, `container`, `system` or `network`.


[^ref2]: https://docs.docker.com/engine/reference/commandline/build/
[^ref3]: https://docs.docker.com/compose/
[^ref4]: https://docs.docker.com/engine/reference/commandline/run/