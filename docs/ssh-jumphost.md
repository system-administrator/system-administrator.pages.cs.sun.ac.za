# SSH Jumphost and useful config tips

To use the CS SSH Jumphost, you will need to have been given access, and you will need to have added your key to the KeyMan website.


Either a lecturer will let you know that you have access, or you will have had to [requested access](https://servicedesk.sun.ac.za/jira/plugins/servlet/theme/portal/11/create/281).

!!! info
    The below guide assumes you know what you are doing with SSH. Some more info can be found on the [NARGA website](https://narga.sun.ac.za/ssh-howto/). Please read that and ask demis for help before sending questions to the admin.

## KeyMan website - Public Key Upload

In order to use the Jumphost, you will first need to upload your public key on the [KeyMan website](https://keyman.cs.sun.ac.za/).

!!! info
    If you need help creating the SSH keys, please follow ["Generate a new SSH key"](https://system-administrator.pages.cs.sun.ac.za/gitlab/git-beginners/#generate-a-new-ssh-key).


## SSH Jumphost Configuration Setup

The SSH jumphost allows a user to connect to a (D)estination host (usually behind a firewall) via a  (B)astion host, otherwise known as a *jump host*. In our case, this **B** host can only be used to jump to certain **D** hosts inside the University network.

!!! note
    If you have been told you have access, you are allowed to use this server for access to the NARGA cluster. You will use the same jump host and destination host as we use in the examples below.

In our examples below, host **D** (destination) will be *open.rga.stb.sun.ac.za* and host **B** (jump host) will be *hermes.cs.sun.ac.za*.

### Command line usage
You can simply mention the jumphost in your normal SSH terminal command:

*Syntax:*
``` console
ssh -J <US_Username>@<jump_host> <US_Username>@<final_host>
```

*Example:*
``` console
ssh -J 12345678@hermes.cs.sun.ac.za 12345678@open.rga.stb.sun.ac.za
```

### SSH config file
You can also setup the hosts in a config file so it's simpler to ssh to the final destination. One way to do this is by creating a `~/.ssh/config` file with contents like the following. Remember to replace the `<>` bits with your own info.

*Syntax:*
``` linux-config linenums="1"
### Jumphost Config. Directly reachable
Host jump
  User <US_Username>
  HostName <jump_host>

### Host to jump to via jump
Host server_name
  HostName <final_host>
  User <US_Username>
  ProxyJump jump
```

*Example:*
``` linux-config linenums="1"
Host jump
  User 12345678
  HostName hermes.cs.sun.ac.za

Host OpenNARGA
  HostName open.rga.stb.sun.ac.za
  User 12345678
  ProxyJump jump
```

Then you can use the config by simply calling `ssh <server_name>`.

*Example:*
``` console
ssh OpenNARGA
```

## Other SSH tips and config

!!! note
    In the following section, it is assumed you have already setup the config file.

### Connecting and reconnecting
Sometimes the connection get's interupted or times out. The session will then kick you out and print a message something like `broken pipe`. You can simply reconnect, however other times the session will hang, and everything you type will seemingly be ignored.

A consequence of this is that whatever you were running (if you didn't run in it something like [screen](#screen)) is now lost to the void. Some things can be recovered, and some processes will run in the background until they exit, but it is far safer to use [screen](#screen) to manage your tasks.

In order to to escape and quit a session that has stopped responding, you can use a keyboard sequence.

1. Press the key `Enter`,
2. Then type tilde,
    * Usually `shift + ~` keys.
    * The Tilde is usually the key just below escape.
3. Finally press the `.` key.

Then, you can simply SSH again.

### Screen
[Screen](https://www.gnu.org/software/screen/) is a useful tool for creating and sustaining multiple sessions on a remote host between disconnects, say, over a temperamental Cellphone connection.

Here we will list some basic usage, more advanced usage should be sought out from your Lecturer or the [screen documentation](https://www.gnu.org/software/screen/manual/).

#### Create sessions
Once an SSH session has been established with a remote host, the user can simply issue the `screen` command to start a screen session.

#### Reconnect a session
When you've been disconnected and had to reconnect, you will want to resume your Screen session.

This can be done by issuing the `screen -r` command, if you only have one screen session. Otherwise, see the following.

#### List sessions
You can list all your screen sessions by typing `screen -l`.

Now, you can issue `screen -r <session number>` to resume that session.

### `rsync`, `scp` and similar

Since SSH is the underlying protocol for many other programs, you can easily use the config above with those programs.

For example, [`rsync`](https://en.wikipedia.org/wiki/Rsync) is a program that allows you to transfer files from one place to another, even on remote hosts.

*Syntax:*
``` console
rsync -[OPTIONS] SRC/s DEST
```

*Example:*
``` console
# To copy a file TO OpenNARGA FROM your local machine
rsync -av /my/file/or/folder/path OpenNARGA:~/home/folder/path

# To copy a file FROM OpenNARGA TO your local machine
rsync -av OpenNARGA:~/home/folder/path /my/file/or/folder/path
```

!!! note
    The flags `av` in `-av` are the rsync flags for "<b>a</b>rchive mode" and to "increase <b>v</b>erbosity".
    Archive mode does a recursive copy, and tries to preserve most file and folder attributes.
    See the rsync man page for more help. (e.g. `man rsync` on a linux machine)


### Port Forwarding

You can port forward a local port to the destination host.

*Syntax:*
``` console
ssh -L<some local port>:localhost:<some_remote_port> server_name
```

*Example:*
``` console
ssh -L 8443:localhost:443 server_name
```

This will allow you to view some webapp running on port 443 on the remote host on `localhost:8443 ` in your local browser.

### File System Mounting

You can view the remote filesystem with `sftp`. For example, in Nautilus (linux file manager), you can connect to a remote servers file system.

*Example:*
``` console
sftp://server_name/home/<user_name>
```

### Opportunistic SSH session reuse
Sometimes a user needs to open many SSH sessions at the same time, or, needs to open SSH sessions one after the other.

This means an SSH tunnel needs to be created and destroyed for each session. This adds delay to scripted tasks, and there is often a rate-limit applied on the number of SSH tunnels on certain networks.

However, we can avoid creating new tunnels for each session with a few handy lines in the SSH config file. This will allow connections to the same destination to reuse an existing tunnel, and to keep this tunnel around even when the initial session has been closed.

Below is a common example for opportunistic SSH session Multiplexing. You can look online for other examples for this setup, or read the docs: https://man.openbsd.org/ssh_config

``` linux-config linenums="1"
Host *
  ControlPath ~/.ssh/controlmasters/%r@%h:%p
  ControlMaster auto
  ControlPersist 10m
```

!!! Note
    You will need to `mkdir ~/.ssh/controlmasters/` before your first time use.

!!! Note "About exiting"
    Sometimes you need to end an open tunnel that has hung, like when changing networks. See here: https://unix.stackexchange.com/a/24013

### Dynamic Jumphost Config
When you have a machine that goes in and out of the University network, like a Laptop, or a machine with a VPN, it's really helpful to dynamically decide if you need the SSH Jumphost or not. 

In my setup, the below config will automatically decide if you need to use the jumphost or not when trying to connect to an university IP.

!!! warning
    This may need some tweaking to work in your setup. GitLab 

!!! Note 
    This seems to work in 20.04 and newer, but not before. 

``` linux-config linenums="1"
Match final host "146.232.*,*.sun.ac.za" !exec "ip route | grep -qF 'default via 146.232.' || ip route | grep -q '146.232.0.0/16 via 146.232.67.'" !host "git.cs.sun.ac.za,hermes.cs.sun.ac.za" 
  HostName %h
  ProxyJump <jumphost>
```



















