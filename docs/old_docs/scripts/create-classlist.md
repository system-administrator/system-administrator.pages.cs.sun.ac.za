# Create classlist from gitolite config file

There may be times when your gitolite config file is the most up-to-date class list you have.
This one liner should help you generate a class list for use with the [git clone script](get-repos-for-marking.md)

The below example will give you a list of students who have repos for *project2*, from the __gitolite.conf__ file in the `gitolite-admin/` repo.

```
cat gitolite.conf | grep '/project2' | cut -d' ' -f2 | cut -d'/' -f1 > class.list
```

Replace the `/project2` with your tut/project repo name.
