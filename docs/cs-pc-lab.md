# CS Postgraduate Lab

The CS Computer lab (A507) is one of the main places for Computer Science Postgrads to get work done. 

## Lab Layout
This is the lab layout for the beginning of 2023. 
There is no guarantee that this will remain like this throughout the year. 

!!! info
    There will also be an overflow room with Hot Seats, with and without computers.
    This will be announced later. 

| Seats | Description |
| --- | --- |
| 1, 8 to 37 | Assigned seats - Grey and Green  |
| | Hot Seats with PC |
| 4, 38 to 45 | Hot Seats for laptop |
| 2, 3, 5, 6 | reserved for MSc students |
| Red | Not available |

![Lab Layout](/assets/images/lab_layout.jpg)

## Lab Computer Usage

All the lab machines will have Ubuntu 22.04 installed initially. Once we have assigned seats, students may request sudo rights, and may even install their own OS. 

If you simply need software installed and you are in a hot seat, or just want to check something before using sudo, please [raise a ticket here](https://servicedesk.sun.ac.za/jira/plugins/servlet/theme/portal/11/create/334).

!!! important
    * You should use **<US number>@su**. That is `...@su`, NOT `...@sun.ac.za`. The latter will work, but will take longer to login. 
    * This is because of how Gnome and Systemd handle usernames that are only numeric.  

## Storage

!!! warning "Data wipe"
    The storage will be wiped at the end of the year, so you need to save any data you want before you leave.
    If you need to continue hons, or are moving to an MSc and use the same PC, please raise a ticket to preserve the machines home data. 

The home drives of the machines are not on the network, so you will need to backup your work and transfer it to different PC's if you move around the lab. It is therefore advised that you try sit at the same machine to work at each time. 


## Bring Your Own Device

If you bring your own machine, be it laptop or Desktop, please make use of the power points (only for these purposes) and Eduroam. 

If you want to use the LAN you will need to [raise a ticket here,](https://servicedesk.sun.ac.za/jira/plugins/servlet/theme/portal/11/create/334) with your OS, MAC address of your ethernet port, and preferred hostname, if you have one. I will not register Windows machines for students directly, you will need to go via the IT hub. 

We may have some screens available for you to use, again, this will need to be a request on the service desk.
