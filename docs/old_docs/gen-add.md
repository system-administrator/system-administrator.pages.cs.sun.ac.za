# How to generate your ssh-key, and add it to the GitLab server

## Beginning steps
Open up your favorite terminal.
## Public key Generation
  Below, you will generate a private and public key for yourself, for use with [`ssh`](http://linuxcommand.org/man_pages/ssh1.html).

  * `ssh-keygen -t rsa -b 4096`
  * Accept the default location (press `Enter`).
  * Put in your own password.
  * Confirm your password.
  * You have now generated a private and public-key pair in the `~/.ssh/` directory.

## GitLab
Copy your new public key to the server.

  Note: replace `<student_number>` with your student number.

* First, get your public key

`gedit /home/<student_number>/.ssh/id_rsa.pub`

* Copy everything in the file.
* Close the file. Make sure you have not changed anything!
* Login to your GitLab account and navigate using the icon on the top right to *Settings*
* Navigate to *SSH Keys*
* Paste the text you copied earlier into the *key* section.
* Add a useful title. e.g. "Campus Computers"
* Finally click *Add Key*

You are now done!
