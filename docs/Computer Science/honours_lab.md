!!! warning
    This page is outdated. W.I.P 

## CS useful links
Other than the main CS webpage, you may find the below useful. Also, checkout the links that have been [embedded](https://www.grammarly.com/blog/imbed-embed/) in the below info. Some may be of interest to you.

### [git.cs.sun.ac.za](https://git.cs.sun.ac.za)
The Computer Science GitLab server. You may already know about this from undergrad. This year, you will be granted permission to create projects there.

### [chat.cs.sun.ac.za](https://chat.cs.sun.ac.za)
The RocketChat server (Log in with the *"Click me!"* button), for IM and topic related group conversations. I am trying to get more users on here, so please join! You can message me by looking for `@ajcollett`.

## Lab Space
The Honours lab is awesome. Just accept it. Below are a few details about it's awesomeness.

### Work Stations
Please keep these areas clean and tidy, but other than that, setup your station as you would like. There are limited resources, so unfortunately not everyone will have everything. However, if something breaks or you think you could do with a new mouse/keyboard/screen please contact me as above.

There is one network cable and *maybe* extra plug points. You may request help if this setup does not suffice.

### PC Setup
For those who use the CS supplied Computers, here is some info about the setup. 

??? info 
    2019 is a bit different to previous years. The honours machines are in part Continuous Integration setups for GitLab. 
    That means that there is a GitLab runner on each machine, which may deploy restricted docker containers for the CI system at any point in time.

!!! warning "Important"
    Please leave the machines powered on. You are welcome to restart them, but we can't do CI if the machines are off. Contact me if you have any questions/concerns.

1. OS: The machines have Ubuntu 18.04 installed
2. Login: Please use your university credentials to login
3. Installed Programs: 
    There is a restricted software catalogue installed. Don't worry, you are welcome to install more software. 
    1. Python 2 and 3
    2. Pip 2 and 3
    3. Full Tex Live
    4. Docker: Docker is installed for the CI system, and setup to use a image proxy (no INETKEY needed for Docker). Once again you will need sudo if you want to use Docker. 
4. Sudo access: Please email me your US number and your PC hostname, and I will give you sudo rights on your machine.
6. NTP, DNS and Apt: These have been setup and customized for campus use.  

!!! warning
    It is a good idea to email about sudo access early on. 
    You will need it to install the software you want to edit and run your projects and code. 

## Printers
There is a printer in the Lab that is avaliable for the Honours students. If you need to print in colour please contact Hayley. Below are the details for the Lab printer.

!!! warning
    The printer should be installed as "Hons Lab (M603)" on all the Honours Lab machines already

| | |
| ---     | --- |
| IP      | 146.232.212.18 |
| Make    | HP |
| Model   | Laserjet 600 M603 |
| Drivers |   [Link](https://support.hp.com/us-en/drivers/selfservice/hp-laserjet-enterprise-600-printer-m603-series/5145286)|


## Something missing?
Please email me what you think should be included at `ajcollett@sun.ac.za`.
