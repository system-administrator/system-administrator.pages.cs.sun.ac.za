## Video/Audio compression

### FFMPEG
For the compression and modification of audio and video files we can use a command line utility called `ffmpeg`.

??? info
    I am not 100% sure on OS support for ffmpeg, but most *nix systems should have a version in their repositories, otherwise [look at the ffmpeg website](https://www.ffmpeg.org/). 

#### Video to audio only with size reduction
Strip all video, unify audio output and reduce the bitrate to an acceptable level.

!!! hint "`tl;dr`"
    `ffmpeg -i <input_file_name> -c:a libfdk_aac -ac 1 -b:a 32k -vn <output_filename>.m4a`

??? note
    In practice, with my suggested settings below, this reduced a 100M M4A file to 22M MP3 file, and 12M M4A file. I couldn't hear the difference, but one can reduce the bit-rate more for smaller (lower quality) files.  

1. Create your audio/video recording in whatever way you want.
1. Decide if you want MP3 or M4A(AAC) output format. I suggest M4A, but provide an MP3 option to students too, since there might still be some devices that cannot play M4A(AAC). AAC audio encoding results in better quality at lower bitrates, resulting in potentially smaller files for the same "quality". 
1. For M4A format, run something like this: 
    
    ```
    ffmpeg -i <input_file_name> -c:a libfdk_aac -ac 1 -b:a <bit_rate> -vn <output_filename>.m4a
    ```

1. For MP3 run something like this: 
    
    ```
    ffmpeg -i <input_file_name> -f mp3 -ac 1 -ab <bit_rate> -vn <output_filename>.mp3
    ```

    1. `-ac 1`: for number of audio channels to 1 (mono)
    1. `-vn`: Audio Only
    1. `-b:a` | `ab`: set the bitrate. 64K for mp3 and 32k for aac seems to work well.
    1. `-c:a libfdk_aac` | `-f mp3`: set encoding.

### Audacity
A great GUI application to use is Audacity. You can achieve the same as above and more with it like noise removal, but it requires more interaction and learning. 

See [their website](https://www.audacityteam.org/) for install options and documentation. Again, there is probably a version in your package repository already. 

