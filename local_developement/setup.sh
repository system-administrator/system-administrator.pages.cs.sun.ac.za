echo "Install pip pacakges" 
pip install --upgrade mkdocs-minify-plugin mkdocs-material mkdocs-git-revision-date-localized-plugin mkdocs-redirects mkdocs-macros-plugin

echo "You must install yq from here https://github.com/mikefarah/yq/releases/latest"

echo "Get the default mkdocs config"
git clone -n https://git.cs.sun.ac.za/websites-hosts-admin/templates.git --depth=1 ./MKDOCS_TEMP
git --git-dir ./MKDOCS_TEMP/.git checkout HEAD sun_theme/assets
git --git-dir ./MKDOCS_TEMP/.git checkout HEAD sun_theme/default_mkdocs.yml
cp -r sun_theme/assets/* ../docs/assets/

mkdir ../docs/assets/fonts
workingdir=$(pwd)
cd ../docs/assets/fonts
curl -O https://www.sun.ac.za/Style%20Library/SUN/Fonts/Raleway-Bold.ttf
curl -O https://www.sun.ac.za/Style%20Library/SUN/Fonts/Raleway-Bold.woff
curl -O https://www.sun.ac.za/Style%20Library/SUN/Fonts/Raleway-Bold.woff2
curl -O https://www.sun.ac.za/Style%20Library/SUN/Fonts/Raleway-Bolditalic.ttf
curl -O https://www.sun.ac.za/Style%20Library/SUN/Fonts/Raleway-Bolditalic.woff
curl -O https://www.sun.ac.za/Style%20Library/SUN/Fonts/Raleway-Bolditalic.woff2
curl -O https://www.sun.ac.za/Style%20Library/SUN/Fonts/Raleway-Italic.ttf
curl -O https://www.sun.ac.za/Style%20Library/SUN/Fonts/Raleway-Italic.woff
curl -O https://www.sun.ac.za/Style%20Library/SUN/Fonts/Raleway-Italic.woff2
curl -O https://www.sun.ac.za/Style%20Library/SUN/Fonts/Raleway-Regular.ttf
curl -O https://www.sun.ac.za/Style%20Library/SUN/Fonts/Raleway-Regular.woff
curl -O https://www.sun.ac.za/Style%20Library/SUN/Fonts/Raleway-Regular.woff2

cd $workingdir

# rm -r sun_theme
rm -rf MKDOCS_TEMP