# Git LFS 

## Intro and Important Info

Git LFS is a way to use git source control for storing certain types of files that usually don't work well as git source content. 

The <b>L</b>arge <b>F</b>ile <b>S</b>torage makes it possible to store image, PDFs, audio and video as well as any binary files. 

In our GitLab instance, these files are not stored on the same server as the other git source material. 

When large files are added to a repository, even in LFS, these files can slow down the performance of the repository in certain situations. 
Before deciding on using Git LFS for these types of files, you should consider whether or not it's worth it. 

!!! warning 
    Setup git LFS when the repository is created! It's very easy.

    However, once you have added files to git source control (not LFS) and pushed these changes to the remote, it is non-trivial to cleanup the remote. 
    
    In this case, you'd have to contact the GitLab admin and ask for help cleaning up the repository. 

    Most repositories are small. However we have had users add full movies to their source. 
    :person_facepalming_tone1: :person_facepalming_tone2: :person_facepalming_tone3: :person_facepalming_tone4: :person_facepalming_tone5:
    
    If your normal git repository (not LFS) starts taking up a lot of space (think anything above 0.4 Gigs) I **WILL** find you.
    
    When it comes to git.cs.sun.ac.za, I see all. 


The main question to consider when deciding if a file should be in the GitLab repository at all is:

> Is the content actually *source*?

Git is for source control. So it must be established whether or not the files to be stored should be considered source. 

**For example:**

* A website may requite small images and icons to be displayed.

    These files make sense to keep in source. 
    If there are many such images and documents, then these items should certainly be added to git LFS, and NOT to the normal git source. 

* A set of code that produces dataset may requite images to produce a result. 

    Ideally, these images as well as the results should not be stored with the source code, however there are exceptions, which I'll get to later.

    * When one has easy online access to these sources, then linking them from the repository is enough. You can even provide a script to help download them.
    * When results of code need to be stored, they should be stored on something like SharePoint, or some other official place that is University controlled. 
    
    Exceptions:
    
    * When the files are small, and small in number.
    * If for a project, and the Supervisor requires the files to be stored in GitLab.
    * There is no other way to store the items.

* A repository is used for research and needs to store PDF's.

    Again, one can make use of other sharing services. 
    However, it also makes sense to keep the PDF's with the rest of the research. 
    In this case, one should carefully choose what papers they store, then store them in git LFS.

## How-to

Using Git LFS on our GitLab instance is simple, and is similar to any other GitLab or git repository, so you can follow [the GitLab help for this.](https://git.cs.sun.ac.za/help/topics/git/lfs/index)
