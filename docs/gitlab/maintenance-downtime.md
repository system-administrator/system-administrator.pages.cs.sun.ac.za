# Maintenance & Downtime

All the large maintenance and downtime dates for git.cs.sun.ac.za should be documented here.

## January/February 2023
Yearly Cleanup of the GitLab instance

### Overview
#### Old repo's to be archived and removed
Repositories that have not seen use for an extended period of time will be archived and removed from GitLab.
   
1. For user created repositories, this will only be done after communicating with the user, unless they are no longer affiliated with the University.
2. For Student repo's created for courses, this will be done automatically. So if you have code in a repo older than 1 year, back it up locally.

Since these will be archived and removed from GitLab, access to these repo's will no longer be possible through gitlab. Thus, users should contact the admin if they need a copy of their project. This may be denied depending on what the Lecturer administering the course says. 

!!! Note
    If the project was created in 2022, it will remain on GitLab, but if it was created before the beginning of 2022, it will be removed. 

#### Old users to be blocked
Users who have not had any activity for more than a year on the instance will be blocked. They can be unblocked, but only through contact with the admin.  

### Plan of Action
1. A full backup will be taken of GitLab.
1. Users with no activity for more than the past 1 year will be disabled.
1. Project removal:
    1. Full backups will be taken of admin created student-projects older than 1 year and archived.
    1. Full backups will be taken of user created projects, in consultation with the user and archived.
    1. All the projects from above will be removed from the GitLab instance.


## January/February 2022
Yearly Cleanup of the GitLab instance

### Overview
#### Old repo's to be archived and removed
Repositories that have not seen use for an extended period of time will be archived and removed from GitLab.
   
1. For user created repositories, this will only be done after communicating with the user, unless they are no longer affiliated with the University.
2. For Student repo's created for courses, this will be done automatically. So if you have code in a repo older than 1 year, back it up locally.

Since these will be archived and removed from GitLab, access to these repo's will no longer be possible through gitlab. Thus, users should contact the admin if they need a copy of their project. This may be denied depending on what the Lecturer administering the course says. 

!!! Note
    If the project was created in 2021, it will remain on GitLab, but if it was created before the begining of 2021, it will be removed. 

#### Old users to be blocked
Users who have not had any activity for more than a year on the instance will be blocked. They can be unblocked, but only through contact with the admin.  

### Plan of Action
1. A full backup will be taken of GitLab.
1. Users with no activity for more than the past 1 year will be disabled.
1. Project removal:
    1. Full backups will be taken of admin created student-projects older than 1 year and archived.
    1. Full backups will be taken of user created projects, in consultation with the user and archived.
    1. All the projects from above will be removed from the GitLab instance.



## 8th September 2021

### What happened?
**The time was out by +2 hours between 6:54 am and 10:07 today, Sep 8.**
There was an error on the VM host. This will have affected all commits and pipelines pushed/run during that time. 

### How does this affect me?
* Commits made during this time will reflect the incorrect time pushed as being 2 hours later than it actually was. 
* Pipelines will reflect the incorrect time for when they were run, and may have failed outright, due to the tockens being invalid for the time on the runners.  


### Are things OK now?
The time is now correct and GitLab should function as normal.

Please log a ticket on the NARGA helpdesk at the GitLab item if you have questions or need help with issues resulting from this. 


## 4th to the 8th January 2021
Yearly Cleanup of the GitLab instance

### Overview
#### Old repo's to be archived and removed
Repositories that have not seen use for an extended period of time will be archived and removed from GitLab.
   
1. For user created repositories, this will only be done after communicating with the user, unless they are no longer affiliated with the University.
2. For Student repo's created for courses, this will be done automatically. So if you have code in a repo older than 1 year, back it up locally.

Since these will be archived and removed from GitLab, access to these repo's will no longer be possible through gitlab. Thus, users should contact the admin if they need a copy of their project. This may be denied depending on what the Lecturer administering the course says. 

!!! Note
    If the project was created in 2020, it will remain on GitLab, but if it was created before the begining of 2019, it will be removed. 

#### Old users to be blocked
Users who have not had any activity for more than a year on the instance will be blocked. They can be unblocked, but only through contact with the admin.  

### Plan of Action
1. A full backup will be taken of GitLab.
1. Users with no activity for more than the past 1 year will be disabled.
1. Project removal:
    1. Full backups will be taken of admin created student-projects older than 1 year and archived.
    1. Full backups will be taken of user created projects, in consultation with the user and archived.
    1. All the projects from above will be removed from the GitLab instance.

## January/February (2020)
Yearly Cleanup of the GitLab instance

### Overview
#### Old repo's to be archived and removed
Repositories that have not seen use for an extended period of time will be archived and removed from GitLab.
   
1. For user created repositories, this will only be done after communicating with the user, unless they are no longer affiliated with the University.
2. For Student repo's created for courses, this will be done automatically. So if you have code in a repo older than 1 year, back it up locally.

Since these will be archived and removed from GitLab, access to these repo's will no longer be possible through gitlab. Thus, users should contact the admin if they need a copy of their project. This may be denied depending on what the Lecturer administering the course says. 

!!! Note
    If the project was created in 2019, it will remain on GitLab, but if it was created before the begining of 2019, it will be removed. 

#### Old users to be blocked
Users who have not had any activity for more than a year on the instance will be blocked. They can be unblocked, but only through contact with the admin.  

### Plan of Action
1. A full backup will be taken of GitLab.
1. Users with no activity for more than the past 1 year will be disabled.
1. Project removal:
    1. Full backups will be taken of admin created student-projects older than 1 year and archived.
    1. Full backups will be taken of user created projects, in consultation with the user and archived.
    1. All the projects from above will be removed from the GitLab instance.

## 9th to 15th December, 2019
Git.cs.sun.ac.za will be going through many changes.

### Overview
#### Migration of the main GitLab instance to IT infrastructure
The GitLab server will be moving, this will make it more easily reachable from just about anywhere.

#### Migration of artifacts and [git LFS (Link)](https://git-lfs.github.com/) objects to S3 storage
GitLab has grown, and so has it's storage needs. Thus, we will be using S3 storage on a seperate server going forward, for pipeline build artifacts, and large items in the Git Repo. Read more about [Git LFS here](https://git.cs.sun.ac.za/help/workflow/lfs/manage_large_binaries_with_git_lfs.md). This will be used mainly for PDFs, pictures, image files and any other binary type, large file.

### How this will affect users
#### Git and all associated operations
git.cs.sun.ac.za will not be operational at all during this time. Therefore no clone/push/pull operations will function. However, you may work on your local copy happily, and commit code as per normal.

This also means that websites deployed from GitLab will remain up, but cannot be updated during this time.

#### Gitlab Pages
GitLab pages may be offline intermittently. Then over the IT maintenance weekend, it will likely be down for an extended period of time.

#### Users with large projects and projects with lots of binary files
Users that have large objects in their projects will be requested to use git LFS going forward. This is currently only possible via the command line, but there are [issues on GitLab.com](https://gitlab.com/gitlab-org/gitlab/issues/20654) to make it available via the web IDE too.

### Plan of Action (Starting **9th early morning**)
1. GitLab will be taken into offline/maintenance mode.
1. A full backup will be taken of GitLab.
1. Artifact migration:
    1. Artifacts older than 6 months will be removed from the instance, but the last 3 artifacts will be kept regardless of age, per repo.
    1. Remaining artifacts will be migrated to object storage.
1. Git LFS migration
    1. Current LFS items will be migrated to object storage.
    1. Certain websites and large repositories will be targeted to move some items to git LFS. This will be done in consultation with the project owner. Find the procedure [here](https://docs.gitlab.com/ee/topics/git/migrate_to_git_lfs/index.html).
1. GitLab Migration:
    1. A final full backup will be taken.
    1. This will then be used to restore the instance on the new server.
    1. Domain names to be changed to correctly point to the new server, and old (now backup) server.
1. Take GitLab out of maintenance mode.
