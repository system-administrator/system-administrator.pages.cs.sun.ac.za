# Get student repos for marking

Getting the repos for all of your students, either as a lecturer or as a demi will be an absolute pain, if you do not have some sort of script for this.

Below is a script I heavily modified, from the source in the script.

This script will need:

* To be copied to a file `get_projects.sh`
* A class list with a list of student numbers, each on it's own line, named `class.list`.
  * [Generate class.list from gitolite.conf](create-classlist.md)

This script will do the following:

* Get all repos for all students in the class list for a particular project.
* Get the *commit log* for each student, and place it in the cloned repo.
* Skip directories that already exist (either delete them or pull in the directory)
* If you enable it, it will also delete the `.git` directory. This is not enabled by default!

Execute with:
> sh get_projects.sh

__Warning!__ The below comes with no guarantees. It __should__ be safe, but if you do not at least have a good grasp of what is going on, [please ask for help!](mailto:ajcollett@sun.ac.za)

```bash
#!/bin/bash
# this code is heavily modified from http://bobbelderbos.com/2012/07/simple-bash-script-to-clone-remote-git-repositories/

# Set the below to your particular class
remoteUser=rw214-2016
# This is the particular Project you want to clone
remoteProject="project2"

# The class list. You will need to create this.
# There should be another script on this wiki to create it if you have access to the admin-repo
# Otherwise ask your lecturer
remoteStudents=$(cat class.list)
remoteHost=git.sun.ac.za

# Where you want things stored
localCodeDir="./studentRepos/"$remoteProject
rootDir=$(pwd)

# For each student in the class list
#  Clone their repo to /'remoteProject'/'studentNumber'
#  Get the git log, and store it in /'remoteProject'/'studentNumber'/gitLog.txt
for student in $remoteStudents
do
  localRepoDir=$(echo ${localCodeDir}/${student})
  if [ -d $localRepoDir ]; then
		echo -e "Directory $localRepoDir already exits, skipping ...\n"
	else
		cloneCmd="git clone $remoteUser@$remoteHost:$student/"
		cloneCmd=$cloneCmd"$remoteProject $localRepoDir"
		cloneCmdRun=$($cloneCmd 2>&1)

    cd $localRepoDir
    git log 2>&1 > gitLog.txt
    # The below will remove the .git folder. But it's commented out.
    # rm -rf will remove (-r)ecursively and ignore (-f) warnings!
    # If you know what your doing, then use it.
    # rm -rf .git
    cd $rootDir

		echo -e "Running: \n$ $cloneCmd"
		echo -e "${cloneCmdRun}\n\n"
    echo -e "Git log saved to $localRepoDir, containing commit details (time and so on)"
	fi
done
```
