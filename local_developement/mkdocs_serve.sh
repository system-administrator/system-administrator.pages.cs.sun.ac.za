yq '. *+ load("../mkdocs.yml")' sun_theme/default_mkdocs.yml > ../mkdocs_local_dev.yml

sed -i 's|- Home: /$|- HOME: index.md|g' ../mkdocs_local_dev.yml
sed -i -E 's|(: /)([[:alpha:]])|: \2|g' ../mkdocs_local_dev.yml
sed -i 's|/$|.md|g' ../mkdocs_local_dev.yml

sed -i -E 's|(/)(assets/images/SU logo_vertical.svg)|\2|g' ../mkdocs_local_dev.yml
sed -i -E 's|(/)(assets/images/SU corporate_goldwhite_horizontal_without_slogan_RGB-01.svg)|\2|g' ../mkdocs_local_dev.yml
sed -i -E 's|(/)(assets/stylesheets/sun_theme.css)|\2|g' ../mkdocs_local_dev.yml


cd ../

export MKDOCS_MATERIAL_SITE_URL=https://localhost/
export MKDOCS_MATERIAL_REPO_URL=https://localhost
export MKDOCS_MATERIAL_SITE_NAME="GitLab & Admin Documentation"

echo "URL: https://researcher.cs.sun.ac.za/user/ajcollett/vscode/proxy/8000/"

mkdocs serve --config-file mkdocs_local_dev.yml