# VPN: Global Connect, Ubuntu 20.04 and 22.04 guide

There are a few ways to make use of the SU PaloAlto GlobalProtect VPN.
You will find 2 different methods on how to setup the VPN on Ubuntu 22.04 and 20.04 below.

## Network Manager (Ubuntu 22.04 only)

On Ubuntu 22.04 the install and setup proceedure is simple.
Things were not as integrated in 20.04, though you can still use openconnect ([gp-saml-gui](https://github.com/dlenski/gp-saml-gui)) in the terminal.

Tested on Vanilla Ubuntu.

### Install 
Install `network-manager-openconnect-gnome` in the terminal or from **Ubuntu Software**.

``` bash
sudo apt install network-manager-openconnect-gnome
```

<video width=100% autoplay muted loop>
<source src="/assets/videos/install_openconnect.mp4" type="video/mp4">
    Your browser does not support the video tag.
</video>

### Setup and Use
Use the Network Manager interface to setup the VPN.

1. In the *settings->network* UI, add a new VPN with the + icon, and select:
    > **Multi-protocol VPN client (openconnect)**
2. Fill in or change only the following details:
    1. **Name:** This is up to you, *SUN_VPN* is an option.
    2. **Gateway:** `stbvpn.sun.ac.za`
3. Click **Add** in the top right corner. 
4. Connect to the VPN with the slider in either the settings menu, or at the "VPN" section in the Desktop menu. 
5. In the pop-up window, I suggest selecting the "**Automatically start connecting next time**" tick box.
6. Then click "**Connect**" in the top right corner and follow the on-screen prompts to authenticate with your University credentials.
7. Once the MFA process is complete, you may need to click "**Login**" in the bottom right corner of the window. 
8. If it seems to crash or not connect, try again. Sometimes it needs 2 attempts before it works.
9. From the Desktop menu at the top right you can now connect, disconnect, or view the VPN status.

<video width=100% autoplay muted loop>
<source src="/assets/videos/openconnect_settings.mp4" type="video/mp4">
    Your browser does not support the video tag.
</video>

## GlobalProtect-openconnect (Ubuntu 20.04 and 22.04)

!!! note
    This guide is a work in progress, videos will be added later. 


You can install and setup GlobalProtect - Openconnect on 20.04. 

link: https://github.com/yuezk/GlobalProtect-openconnect

### Install

``` bash
sudo add-apt-repository ppa:yuezk/globalprotect-openconnect
sudo apt-get update
sudo apt install globalprotect-openconnect
```

### Setup
You can then open the GUI and try connecting with the following details: 

* Server: stbvpn.sun.ac.za 
* Login: Your University details
