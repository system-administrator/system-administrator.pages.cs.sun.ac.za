# Shared Runners Config

## Enabling Shared Runners
The CS GitLab system has a number of shared runners that students can make use of. 

To enable "shared runners" a user should turn them on in the settings of their project. If your project is managed for you, you may need to ask the Lecturer to do this. 

The shared runners also require the use of [tags](https://docs.gitlab.com/runner/#tags) for the runner to pickup your job.

There are a few tags that users can make use of, and they can be viewed in the "runners" section of a projects "settings".

Below, a picture showing where to find the "enable" button and the shared runners tags.
![Login Page](/assets/images/runner_tags.png)


## Using Shared Runners
The shared runners on CS GitLab are docker runners, this means you will need to use a docker image with the job, or a default one will be used. 

See the gitlab docs for more info about the CI/CD file: https://git.cs.sun.ac.za/help/ci/yaml/index.md


## Further info
Please submit suggestions for this page at the link in the bar on the left.
